Acquia Extras module
================================================================================

This module provides some additional functionality to sites hosted on the Acquia
Cloud hosting platform [1]. This module is maintained by Third and Grove [2].
Contact josh@thirdandgrove.com if you have questions about this module.

[1] https://www.acquia.com/products-services/acquia-cloud
[2] http://www.thirdandgrove.com
