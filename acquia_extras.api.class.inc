<?php

/**
 * @file
 * Class definition for Acquia Cloud API calls
 */

/**
 * Application-related class to manage requests to Acquia Cloud API
 */
class AcquiaCloudAPI {
  private $endpoint = 'https://cloudapi.acquia.com/v1';
  private $conf = array();
  private $curl_info;
  private $curl_error;
  private $curl_response;

  /**
   * Constructor to build the configuration settings
   *
   * @access public
   * @return void
   */
  public function __construct() {
    $this->conf['env'] = variable_get('acquia_env');
    $this->conf['site'] = variable_get('acquia_site');
    $this->conf['domain'] = variable_get('acquia_domain');
    $this->conf['user'] = variable_get('acquia_username');
    $this->conf['pass'] = variable_get('acquia_password');
  }

  /**
   * Send a GET request to get the domain name
   *
   * @access public
   * @return boolean
   *   TRUE if successful
   */
  public function getDomain() {
    return $this->curl('/sites/:site/envs/:env/domains/:domain.json');
  }

  /**
   * Send a DELETE request to purge varnish cache
   *
   * @access public
   * @return boolean
   *   TRUE if successful
   */
  public function purgeVarnish() {
    return $this->curl('/sites/:site/envs/:env/domains/:domain/cache.json', 'DELETE');
  }

  /**
   * Getter to return the parsed API response
   *
   * @access public
   * @return stdObject
   */
  public function getResponse() {
    return json_decode($this->curl_response);
  }

  /**
   * Getter to return the curl response info
   *
   * @access public
   * @return array
   */
  public function getCurlInfo() {
    return $this->curl_info;
  }

  /**
   * Takes a path and an HTTP method, and send the request
   * to the Acquia Cloud API
   *
   * @access private
   * @param string $path
   *   A named parameter string which corresponds to the conf keys
   * @param string $method
   *   A string of the desired HTTP method (GET, POST, DELETE, ...)
   * @return boolean
   *   TRUE if HTTP status code is 200 with no curl errors, otherwise FALSE
   */
  private function curl($path, $method = 'GET') {
    $this->curl_response = FALSE;
    $this->curl_error = FALSE;
    $this->curl_info = FALSE;

    $path = $this->preparePath($path);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $this->endpoint.$path);

    // Set the HTTP method
    if (!empty($method) && $method != 'GET') {
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    }

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
    curl_setopt($ch, CURLOPT_USERPWD, sprintf('%s:%s', $this->conf['user'], $this->conf['pass']));
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);


    $this->curl_response = curl_exec($ch);
    $this->curl_error = curl_error($ch);
    $this->curl_info = curl_getinfo($ch);

    // WATCHDOG if there's a curl error
    $success = TRUE;
    if (!empty($this->curl_error)) {
      $success = FALSE;
      $message = 'Acquia API Error: @error <p>*** INFO ***</p> <pre>@info</pre>';
      $variables = array(
        'error' => $this->curl_error,
        'info' => print_r($this->curl_info, TRUE)
      );
      drupal_set_message('An error happened with the HTTP request.', 'error');
      watchdog('acquia_extras', $message, $variables, WATCHDOG_EMERGENCY);
    }

    // WATCHDOG if there isn't a 200 status code
    if ($this->curl_info['http_code'] != 200) {
      $success = FALSE;
      $message = 'Acquia API HTTP Error: @code <p>*** INFO ***</p> <pre>@info</pre>';
      $variables = array (
        'code' => $this->curl_info['http_code'],
        'info' => print_r($this->curl_info, TRUE)
      );
      drupal_set_message("A {$this->curl_info['http_code']} was returned.", 'error');
      watchdog('acquia_extras', $message, $variables, WATCHDOG_ALERT);
    }

    curl_close($ch);

    return $success;
  }

  /**
   * A method that parses the named parameter path
   *
   * @access private
   * @param string $path
   *   A string with named parameters, which correspond to conf keys
   * @return string
   *   A path with the named parameters substituted out for the conf values
   */
  private function preparePath($path) {
    $matches = array();
    $subs = array();

    // Find the named parameters
    preg_match_all('/:(\w+)/', $path, $matches);

    // Build an ordered array based on the named parameters
    foreach ($matches[1] as $param) {
      $subs[] = $this->conf[$param];
    }

    // Prepare the sprintf version of the path
    $path = preg_replace('/:\w+/', '%s', $path);

    // Return the vsprintf'd string
    return vsprintf($path, $subs);
  }
}
