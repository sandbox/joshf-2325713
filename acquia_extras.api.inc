<?php

/**
 * @file
 * Callbacks for the varnish cache clear
 */

/**
 * Form submit function which sends a GET domain info curl request to the Acquia Cloud API
 */
function acquia_extras_api_test_config() {
  module_load_include('class.inc', 'acquia_extras', 'acquia_extras.api');
  $api = new AcquiaCloudAPI();
  if ($api->getDomain()) {
    $response = $api->getResponse();
    $status = $api->getCurlInfo();
    drupal_set_message('Success', 'status');
  } else {
    drupal_set_message('The configs might not be set correctly.', 'error');
  }
}

/**
 * Form submit function which sends a "purge varnish" curl request to the Acquia Cloud API
 */
function acquia_extras_api_purge_varnish() {

  // Flush everything, otherwise varnish will just pick-up the drupal cache
  drupal_flush_all_caches();

  module_load_include('class.inc', 'acquia_extras', 'acquia_extras.api');
  $api = new AcquiaCloudAPI();
  if ($api->purgeVarnish()) {
    $response = $api->getResponse();
    drupal_set_message('Acquia Varnish cache has been purged', 'status');
  } else {
    drupal_set_message('Failure - Check the logs', 'error');
  }
}
