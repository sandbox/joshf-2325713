<?php

/**
 * @file
 * Specify Acquia environments to show content to or hide content from.
 */

$plugin = array(
  'title' => t('Acquia: Environment'),
  'description' => t('Controls access by Acquia Environment (dev/test/prod).'),
  'callback' => 'acquia_extras_environment_ctools_access_check',
  'default' => array(
    'dev' => 0,
    'test' => 0,
    'prod' => 0,
    'other' => 0,
  ),
  'settings form' => 'acquia_extras_environment_ctools_settings',
  'summary' => 'acquia_extras_environment_ctools_summary',
);

/**
 * Returns TRUE if the current environment matches one of the selected ones.
 */
function acquia_extras_environment_ctools_access_check($conf, $context) {
  if (isset($_ENV['AH_SITE_ENVIRONMENT']) && array_key_exists($_ENV['AH_SITE_ENVIRONMENT'], $conf)) {
    return (boolean) $conf[$_ENV['AH_SITE_ENVIRONMENT']];
  }
  return (boolean) $conf['other'];
}

/**
 * Presents the access plugin's settings form.
 */
function acquia_extras_environment_ctools_settings($form, &$form_state, $conf) {
  $form['settings']['dev'] = array(
    '#type' => 'checkbox',
    '#title' => t('Development'),
    '#default_value' => $conf['dev'],
  );
  $form['settings']['test'] = array(
    '#type' => 'checkbox',
    '#title' => t('Testing/staging'),
    '#default_value' => $conf['test'],
  );
  $form['settings']['prod'] = array(
    '#type' => 'checkbox',
    '#title' => t('Production'),
    '#default_value' => $conf['prod'],
  );
  $form['settings']['other'] = array(
    '#type' => 'checkbox',
    '#title' => t('Non-Acquia environments'),
    '#description' => t('Select this if you want this content to be visible in non-Acquia (ie local) environments.'),
    '#default_value' => $conf['other'],
  );
  return $form;
}

/**
 * Describes the access plugin and its configuration.
 */
function acquia_extras_environment_ctools_summary($conf, $context) {
  $envs = array();
  foreach ($conf as $key => $value) {
    if ($value) {
      $envs[] = $key;
    }
  }
  $env = implode(' OR ', $envs);
  return t('Environment is !environment', array('!environment' => $env));
}
